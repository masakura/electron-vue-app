module.exports = {
  pluginOptions: {
    electronBuilder: {
      externals: ['my-counter'],
      nodeModulesPath: ['./node_modules'],
    },
  },
};
