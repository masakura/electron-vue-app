# Electron Vue.js
Electron + Vue.js でアプリを作る。


#### セットアップ

```
$ cd vue-app
vue-app$ npm install
```

#### 開発

```
vue-app$ npm run electron:serve
```

#### パッケージ化

```
vue-app$ npm run electron:build
```

## メモ
* [Vue CLI Plugin Electron Builder](https://nklayman.github.io/vue-cli-plugin-electron-builder/guide/guide.html) を使うとかなり簡単
  * Node.js 側のモジュールを呼び出すときは [Native Modules](https://nklayman.github.io/vue-cli-plugin-electron-builder/guide/guide.html#native-modules-badge-text-1-0-0-rc-1-type-warn) を参考にホワイトリストを作る
  * ビルドされたアプリが空白ページになるときは [Blank screen on builds, but works fine on serve](https://nklayman.github.io/vue-cli-plugin-electron-builder/guide/guide.html#native-modules) を見る
