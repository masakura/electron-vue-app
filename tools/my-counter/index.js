class MyCounter {
  constructor() {
    this.value = 0;
  }

  increment() {
    this.value++;
  }
}

module.exports = MyCounter;
